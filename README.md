# Third Person Meele Combat Plugin For Unreal Engine 4

This Plugin is heavily based on animations. It aims to provide an easy access for artists and/or game designers via Blueprints thus they are able to implement and test their asset creations in an etablished environment for third person games.

The example project's animations for combat are provided by [Mixamo](https://www.mixamo.com "Mixamo's Homepage") and are retargeting to the Skeleton Mesh of the Third Person Template's Character.

## Adding The Plugin To An Existing C++ Project


Recommendations: Download or clone the entire repository to check out the example project before using the plugin from scratch! If the editor should ask you to disable the plugin when open the .uproject file, deny it and rebuild the missing modules. 

1. Ensure that the Unreal Editor is closed.
2. Create a new folder named "Plugins" next to the u.project file in your project folder structure
3. Copy the MCPPlugin folder to the "Plugins" folder 
4. Start the Unreal Editor and open your project.
5. Ensure under Edit > Plugins > MCP that the plugin is enabled.

## Use 

### Project Setup 

Go to Edit > Project Settings > Input and add the following Action and Axis Mappings:

Action Mappings:

* Jump (e.g. "Space")
* Equip Weapon (e.g. "Q")
* Attack (e.g. "Left Mouse Button")

Axis Mappings:

* MoveForward: A group of two items. Set the scale to 1 to move your character forward and to -1 to move it backwards. 
* MoveRight: A group of two items. Set the scale to -1 to move your character left and to 1 to move it right. 
* Turn: Should be mapped to your mouse's X-axis. Scale is 1. 
* LookUp: Shoule be mapped to your mouse's Y-axis. Scale is -1. 

### Character Setup

1. Create a new Blueprintclass deriving from MCPCharacter 
2. Set the Skeletal Mesh of the Mesh Component 
3. Set the Animation Mode to Use Animation Blueprint and choose your Anim Class 
4. Add a Static Mesh Component and set its Mesh variable to your Weapon's Mesh
5. Add a MCPWeapon Component and set its damage variables (HitVFX, HitSFX, DamageRange, Damage)
6. Add Sockets (e.g. RightHand, Pelvis) to your Character's Skeleton 
7. Set its parameters to ensure a realistic behaviour for holding, drawing and sheathing a weapon (**Tip:** Add a Preview Asset)
8. Attach the weapon's Mesh Component to a default Socket (e.g. Pelvis) and set it to your Character's weapon component variable (Construction Script)
9. Set the Default Pawn Class for your GameMode to your recent created Blueprint

![alt text](https://gitlab.com/pschmidt/ue4-mcp/raw/master/Documentation/Character's_ConstructionScript.png "Character's ConstructionScript setup")

### Animation Setup

To get an idea how to set up the whole Blendspace for attack movement, Aninmation Montages and Notifcations and your Character's Anim Graph check out the example project.

So far the plugin uses the following Animation Notifactions:

* Attack: Triggers the Character's event to start performing an attack combo 
* Save: Triggers the Character's event to save the current state of an attack combo 
* Reset: Triggers the Character's event to reset the current attack combo 

![alt text](https://gitlab.com/pschmidt/ue4-mcp/raw/master/Documentation/AnimationNotificationSetupExample.png "Animation Notifications example setup")

Thus the player has to time the every attack action to achieve progress in his attack combo. 

* Draw: Triggers the Character's event to draw an attached weapon 
* Sheath: Triggers the Character's event to sheath an attached weapon 

Every Animation Notification has to be set up correctly in the Anim Instance's Event Graph to trigger its corresponding event from the MCPCharacter class.

![alt text](https://gitlab.com/pschmidt/ue4-mcp/raw/master/Documentation/AnimNotifyExample.png "AnimNotify example")

### Equip A Weapon 

1. Add a InputAction Equip Weapon to your Character's Event Graph and wire it to the EquipWeapon function 
2. Set its parameters to your draw and sheath weapon Animation Montages 

![alt text](https://gitlab.com/pschmidt/ue4-mcp/raw/master/Documentation/Character's_EquipWeaponAction.png "Character's equip weapon functionality")

3. Add events OnDrawWeapon and OnSheathWeapon (triggered by Animation Notifications) and wire each to them of an AttachToComponent Node
4. Set Target to the weapon's mesh and Parent to the character's Mesh
5. Socket Names should match with the ones you set up earlier

![alt text](https://gitlab.com/pschmidt/ue4-mcp/raw/master/Documentation/Character's_AttachWeaponEvents.png "Draw and sheath weapon events")

### Combo Attacks 

1. Add a InputAction Attack to your Character's Event Graph and wire it to the Attack function 
2. Set its parameters to your three combo attack Animation Montages 

![alt text](https://gitlab.com/pschmidt/ue4-mcp/raw/master/Documentation/Character's_AttackAction.png "Character's combo attack functionality")

## License

Third Person Meele Combat Plugin for Unreal Engine 4 is licensed as follows:

* Everything beneath MCPlugin/Content/StarterContent, as well as the Character's Mesh, Skeleton, Material and Animations for movement are governed by the [Unreal Engine End User License Agreement](https://www.unrealengine.com/en-US/eula)
* Everything else is released under the [MIT License](https://gitlab.com/pschmidt/ue4-mcp/blob/master/LICENSE)

