
#include "Classes/MCPCharacter.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Animation/AnimInstance.h"
#include "Animation/AnimMontage.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"
#include "Runtime/Engine/Classes/Engine/World.h"
#include "Runtime/Engine/Classes/Engine/EngineTypes.h"
#include "Runtime/Engine/Public/CollisionQueryParams.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

//////////////////////////////////////////////////////////////////////////
// AMCPCharacter

AMCPCharacter::AMCPCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 300.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Set default state for weapon sheathed
	bHasWeaponSheathed = true;
}

//////////////////////////////////////////////////////////////////////////
// Input

void AMCPCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AMCPCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &AMCPCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &AMCPCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &AMCPCharacter::LookUpAtRate);
}

/** Turns the character's facing direction */
void AMCPCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AMCPCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

/** Moves the character in forward direction */
void AMCPCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

/** Moves the character in right direction */
void AMCPCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

//////////////////////////////////////////////////////////////////////////
// Equip 

/** Plays an Anim Montage if the character draws oder sheaths a weapon */
void AMCPCharacter::EquipWeapon(UAnimMontage * DrawAnimation, UAnimMontage * SheathAnimation)
{
	bHasWeaponAttached = !bHasWeaponAttached;
	UE_LOG(LogTemp, Warning, TEXT("MyCharacter's Bool is %s"), (bHasWeaponAttached ? TEXT("True") : TEXT("False")));

	if (bHasWeaponAttached == true)
	{
		bUseControllerRotationYaw = true;
		bHasWeaponSheathed = false;

		GetMesh()->GetAnimInstance()->Montage_Play(DrawAnimation, 1.0f);
	}
	else if (bHasWeaponAttached == false)
	{
		bUseControllerRotationYaw = false;
		bHasWeaponSheathed = true;

		GetMesh()->GetAnimInstance()->Montage_Play(SheathAnimation, 1.0f);
	}
}

/** Empty OnDrawWeapon event implementation gets override in BP */
void AMCPCharacter::OnDrawWeapon_Implementation()
{

}

/** Empty OnSheathWeapon event implementation gets override in BP */
void AMCPCharacter::OnSheathWeapon_Implementation()
{

}

//////////////////////////////////////////////////////////////////////////
// Attack

/** Performs combo attacks if the weapon is not sheathed */
void AMCPCharacter::Attack(UAnimMontage * FirstAttackAnimation, UAnimMontage * SecondAttackAnimation, UAnimMontage * ThirdAttackAnimation)
{
	if (bHasWeaponSheathed == false)
	{
		if (bIsAttacking)
		{
			bIsSavingAttackState = true;
		}
		else
		{
			bIsAttacking = true;
			ComboAttack(FirstAttackAnimation, SecondAttackAnimation, ThirdAttackAnimation);
		}
	}
}
/** Plays an Anim Montage depentend on the current combo attack state */
void AMCPCharacter::ComboAttack(UAnimMontage * FirstAttackAnimation, UAnimMontage * SecondAttackAnimation, UAnimMontage * ThirdAttackAnimation)
{
		switch (AttackComboCount) {

		case 0:
			AttackComboCount = 1;
			GetMesh()->GetAnimInstance()->Montage_Play(FirstAttackAnimation, 1.0f);
			break;
		case 1:
			AttackComboCount = 2;
			GetMesh()->GetAnimInstance()->Montage_Play(SecondAttackAnimation, 1.0f);
			break;
		case 2:
			AttackComboCount = 0;
			GetMesh()->GetAnimInstance()->Montage_Play(ThirdAttackAnimation, 1.0f);
			break;
		}
}

/** Triggered by an Animation Notification to perform a Sweep */
void AMCPCharacter::OnAttack_Implementation()
{
	if (WeaponComponent != NULL)
	{
		SweepStart = GetActorLocation();
		SweepEnd = SweepStart + (GetActorForwardVector() * WeaponComponent->DamageRange);
		Rotation = GetActorRotation().Quaternion();

		const FName TraceTag("SightTrace");

		FCollisionQueryParams TraceParams;
		TraceParams.TraceTag = TraceTag; 
		TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = true;
		TraceParams.AddIgnoredActor(this);

		GetWorld()->DebugDrawTraceTag = TraceTag;

		GetWorld()->SweepMultiByChannel(OutResults, SweepStart, SweepEnd, Rotation, ECollisionChannel::ECC_Camera,
			FCollisionShape::MakeSphere(25.0f), TraceParams);

		for (FHitResult OutHit : OutResults)
		{
			UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponComponent->HitVFX, OutHit.Location);
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponComponent->HitSFX, OutHit.Location);

			// Apply Damage here 
		}
	}
}

/** Triggered by Animation Notifacation to save the current combo state and perform a combo attack */
void AMCPCharacter::OnSaveComboAttack_Implementation(UAnimMontage * FirstAttackAnimation, UAnimMontage * SecondAttackAnimation, UAnimMontage * ThirdAttackAnimation)
{
	if (bIsSavingAttackState)
	{
		bIsSavingAttackState = false;
		ComboAttack(FirstAttackAnimation, SecondAttackAnimation, ThirdAttackAnimation);
	}
}

/** Triggered by Animation Notification to reset the combo attack chain */
void AMCPCharacter::OnResetCombo_Implementation()
{
	AttackComboCount = 0;
	bIsSavingAttackState = false;
	bIsAttacking = false;
}

