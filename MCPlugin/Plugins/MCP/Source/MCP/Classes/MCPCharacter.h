// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Classes/MCPWeaponComponent.h"
#include "MCPCharacter.generated.h"

UCLASS()
class AMCPCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	AMCPCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Camera)
	float BaseLookUpRate;

	/** Has the player currently a weapon attached? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Equip")
	bool bHasWeaponAttached;

	/** Has the player currently a weapon sheathed? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Equip")
	bool bHasWeaponSheathed;

	/** Is the player currently attacking? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Attack")
	bool bIsAttacking;

	/** Is it allowed to save the current combo state? */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Attack")
	bool bIsSavingAttackState;

	/** Counts the combo attack states */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Attack")
	int32 AttackComboCount;

	/** The Character's Weapon Component */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Attack")
	UMCPWeaponComponent * WeaponComponent;

	/** Triggered by Animation Notifications to perform attack combos */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "MCP | Attack")
	void OnAttack();
	void OnAttack_Implementation();

	/** Triggered by Animation Notifications to draw a weapon */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "MCP | Equip")
	void OnDrawWeapon();
	void OnDrawWeapon_Implementation();

	/** Triggered by Animation Notifications to sheath a weapon */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "MCP | Equip")
	void OnSheathWeapon();
	void OnSheathWeapon_Implementation();

	/** Triggered by Animation Notifications to save the current combo state */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "MCP | Attack")
	void OnSaveComboAttack(UAnimMontage * FirstAttackAnimation, UAnimMontage * SecondAttackAnimation, UAnimMontage * ThirdAttackAnimation);
	void OnSaveComboAttack_Implementation(UAnimMontage * FirstAttackAnimation, UAnimMontage * SecondAttackAnimation, UAnimMontage * ThirdAttackAnimation);

	/** Triggered by Animation Notifications to reset the current combo */
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "MCP | Attack")
	void OnResetCombo();
	void OnResetCombo_Implementation();

protected:

	/** The FHitResults in OnAttack event */
	TArray<FHitResult> OutResults;

	/** The start and end sweep location in OnAttack event */
	FVector SweepStart;
	FVector SweepEnd;

	/** The Sweepo rotation */
	FQuat Rotation;

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** Called for equip weapon input */
	UFUNCTION(BlueprintCallable, Category = "MCP | Equip")
	void EquipWeapon(UAnimMontage * DrawAnimation, UAnimMontage * SheathAnimation);

	/** Called for combo attack input */
	UFUNCTION(BlueprintCallable, Category = "MCP | Attack")
	void Attack(UAnimMontage * FirstAttackAnimation, UAnimMontage * SecondAttackAnimation, UAnimMontage * ThirdAttackAnimation);

	/** Helper method to play animation montages for combo attacks */
	void ComboAttack(UAnimMontage * FirstAttackAnimation, UAnimMontage * SecondAttackAnimation, UAnimMontage * ThirdAttackAnimation);

	/**
	* Called via input to turn at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void TurnAtRate(float Rate);

	/**
	* Called via input to turn look up/down at a given rate.
	* @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	*/
	void LookUpAtRate(float Rate);

protected:

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:

	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

};
