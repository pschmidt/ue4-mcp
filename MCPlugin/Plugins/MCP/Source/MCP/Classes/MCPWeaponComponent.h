#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MCPWeaponComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class UMCPWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UMCPWeaponComponent();

	/** The VFX is played when hitting other actors  */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Damage")
	UParticleSystem * HitVFX;

	/** The SFX is played when hitting other actos */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Damage")
	USoundBase * HitSFX;

	/** Damage range of the weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Damage")
	float DamageRange;

	/** Damage value when applying damage to other actors */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MCP | Damage")
	float Damage;
};
