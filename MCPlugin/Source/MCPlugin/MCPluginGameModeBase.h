// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MCPluginGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class AMCPluginGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:

	AMCPluginGameModeBase();
	
	
};
